CREATE TABLE If NOT EXISTS statuses (
    id serial primary key,
    name varchar(255)
);

CREATE TABLE IF NOT EXISTS objects (
    id serial primary key,
    name varchar(255),
    status_id integer,
    address varchar(255),
    FOREIGN KEY (status_id) REFERENCES statuses(id)
);

CREATE TABLE IF NOT EXISTS buildings (
    id serial primary key,
    number integer,
    object_id integer,
    floors integer,
    FOREIGN KEY (object_id) REFERENCES objects(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS clients (
   id serial primary key,
   name varchar(255)
);

CREATE TABLE IF NOT EXISTS flats (
     id serial primary key,
     floor integer,
     number integer,
     building_id integer,
     client_id integer,
     FOREIGN KEY (building_id) REFERENCES buildings(id) ON DELETE CASCADE,
     FOREIGN KEY (client_id) REFERENCES clients(id)
);

CREATE TABLE IF NOT EXISTS positions (
    id serial primary key,
    name varchar(255),
    salary integer
);

CREATE TABLE IF NOT EXISTS workers (
    id serial primary key,
    name varchar(255),
    position_id integer,
    FOREIGN KEY (position_id) REFERENCES positions(id)
);

CREATE TABLE IF NOT EXISTS buildings_workers (
    building_id integer,
    worker_id integer,
    FOREIGN KEY (building_id) REFERENCES buildings(id),
    FOREIGN KEY (worker_id) REFERENCES workers(id)
);

INSERT INTO statuses (name) VALUES
    ('Pending'),
    ('In progress'),
    ('Finished');

INSERT INTO objects (name, status_id, address) VALUES
    ('Osnova', 1, 'Poshtova str. 112'),
    ('CBS', 2, 'Shkilna str. 27'),
    ('Kvartal', 3, 'Fortechna str. 49');

INSERT INTO buildings (number, object_id, floors) VALUES
    (1, 2, 20),
    (2, 2, 25),
    (1, 3, 10);

INSERT INTO clients (name) VALUES
    ('Petr Ivanov'),
    ('Sergey Petrov'),
    ('Ivan Sidorov');

INSERT INTO flats (floor, number, building_id, client_id) VALUES
    (2, 7, 2, 1),
    (10, 144, 3, 1),
    (5, 78, 3, 2),
    (7, 95, 3, 3),
    (3, 17, 2, 2);

INSERT INTO positions (name, salary) VALUES
    ('Geodesist', 10000),
    ('Excavator driver', 15000),
    ('Truck driver', 15000),
    ('Foreman', 20000),
    ('Handyman', 7000);

INSERT INTO workers (name, position_id) VALUES
    ('Will Smith', 4),
    ('John Travolta', 5),
    ('Jack Peterson', 5),
    ('Billy Stathem', 1),
    ('Tim Grey', 2),
    ('Alex Jackson', 3);

INSERT INTO buildings_workers(building_id, worker_id) VALUES
    (1, 1),
    (2, 1),
    (3, 1),
    (4, 1),
    (5, 1),
    (6, 1),
    (1, 2),
    (2, 3),
    (3, 4),
    (4, 5),
    (5, 6);

SELECT
   w.name,
   p.name AS position,
   p.salary
FROM workers w
    LEFT JOIN positions p ON w.position_id = p.id;

SELECT
   w.name,
   b.number AS house_number,
   o.name AS object_name,
   o.address
FROM buildings_workers bw
    LEFT JOIN buildings b ON bw.building_id = b.id
    LEFT JOIN workers w ON bw.worker_id = w.id
    LEFT JOIN objects o ON b.object_id = o.id;

SELECT
   f.number AS flat_number,
   f.floor,
   c.name AS owner,
   b.number AS house_number,
   o.address
FROM flats f
    LEFT JOIN clients c ON f.client_id = c.id
    LEFT JOIN buildings b ON f.building_id = b.id
    LEFT JOIN objects o ON b.object_id = o.id;

SELECT o.name, s.name AS status, address FROM objects o
    LEFT JOIN statuses s ON o.status_id = s.id;

SELECT number, floors, address FROM buildings b
    LEFT JOIN objects o ON b.object_id = o.id;
